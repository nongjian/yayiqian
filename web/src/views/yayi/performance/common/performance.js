import {service, t,} from "../../../../http/Request";


/** 公用接口 */
export const performanceApi = {
    page(data){
        return service({
            url:"/api/performance/page",
            method:'get',
            params:data
        });

    },



    getOne(data){
        return service({
            url: "/api/performance/get",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

    delete(data) {
        return service({
            url: "/api/performance/delete",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

    add(data, params) {
        return service({
            url: "/api/performance/insert",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...params,...t},
            data: data

        })
    },
    update(data) {
        return service({
            url: "/api/performance/update",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...t},
            data: data

        })
    }
};
