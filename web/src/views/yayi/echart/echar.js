import {service,} from "../../../http/Request";

const t = {
    t:new Date()
}
/** 公用接口 */
export const echarth = {

    get() {
        return service({
            url: "/api/echart/get",
            method: 'get',
        });
    },

    get2() {
        return service({
            url: "/api/echart/get2",
            method: 'get',
        });
    },
};
