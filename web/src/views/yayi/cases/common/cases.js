import {service,} from "../../../../http/Request";

const t = {
    t:new Date()
}
/** 公用接口 */
export const casesApi = {
    getListById(data){
        return service({
            url:"/api/cases/getListById",
            method:'get',
            params:data
        });

    },
    list(data){
        return service({
            url:"/api/cases/list",
            method:'get',
            params:data
        });

    },
    getOne(data){
        return service({
            url: "/api/cases/get",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },
    page(data){
        return service({
            url:"/api/cases/page",
            method:'get',
            params:data
        });
    },
    add(data) {
        return service({
            url: "/api/cases/insert",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...t},
            data: data

        })
    },
};
