import {service, t,} from "../../../../http/Request";


/** 公用接口 */
export const prescriptionInfoApi = {
    page(data){
        return service({
            url:"/api/prescriptionInfo/page",
            method:'get',
            params:data
        });

    },

    getOne(data){
        return service({
            url: "/api/prescriptionInfo/get",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },
    getlist(data){
        return service({
            url: "/api/prescriptionInfo/getlist",
            method: 'get',
            params: data
        });
    },

    delete(data) {
        return service({
            url: "/api/prescriptionInfo/delete",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

    add(data, params) {
        return service({
            url: "/api/prescriptionInfo/insert",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...params,...t},
            data: data

        })
    },
    update(data) {
        return service({
            url: "/api/prescriptionInfo/update",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...t},
            data: data

        })
    }
};
