/**
 * 模拟数据
 */

export let responseData = {
    "total": 314,
    "rows": [ {
        "id": 1,
        "patientId": 1001,
        "name": "患者A",
        "age": 25,
        "gender": "男",
        "contactInfo": "13800138000"
    },
        {
            "id": 2,
            "patientId": 1002,
            "name": "患者B",
            "age": 30,
            "gender": "女",
            "contactInfo": "13800138001"
        },
        {
            "id": 3,
            "patientId": 1003,
            "name": "患者C",
            "age": 45,
            "gender": "男",
            "contactInfo": "13800138002"
        },
        {
            "id": 4,
            "patientId": 1004,
            "name": "患者D",
            "age": 50,
            "gender": "女",
            "contactInfo": "13800138003"
        },
        {
            "id": 5,
            "patientId": 1005,
            "name": "患者E",
            "age": 28,
            "gender": "男",
            "contactInfo": "13800138004"
        },
        {
            "id": 6,
            "patientId": 1006,
            "name": "患者F",
            "age": 35,
            "gender": "女",
            "contactInfo": "13800138005"
        },
        {
            "id": 7,
            "patientId": 1007,
            "name": "患者G",
            "age": 55,
            "gender": "男",
            "contactInfo": "13800138006"
        }, {
            "id": 8, "patientId": 1008, "name": "患者H", "age": 42, "gender": "女", "contactInfo": "13800138007"
        }, {
            "id": 9, "patientId": 1009, "name": "患者I", "age": 27, "gender": "男", "contactInfo": "13800138008"
        }, {
            "id": 10, "patientId": 1010, "name": "患者J", "age": 47, "gender": "女", "contactInfo": "13800138009"
        }, {
            "id": 11, "patientId": 1011, "name": "患者K", "age": 32, "gender": "男", "contactInfo": "13800138010"
        }, {
            "id": 12, "patientId": 1012, "name": "患者L", "age": 52, "gender": "女", "contactInfo": "13800138011"
        }, {
            "id": 13, "patientId": 1013, "name": "患者M", "age": 49, "gender": "男", "contactInfo": "13800138012"
        },
    ],
    "code": 200,
    "msg": "查询成功"
};
