import {service,} from "../../../../http/Request";

const t = {
    t:new Date()
}
/** 公用接口 */
export const employeeApi = {

    list(data) {
        return service({
            url: "/api/employee/list",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },


    page(data) {
        return service({
            url: "/api/employee/page",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },
    getOne(data){
        return service({
            url: "/api/employee/get",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },
    delete(data) {
        return service({
            url: "/api/employee/delete",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

    add(data, params) {
        return service({
            url: "/api/user/register",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...params,...t},
            data: data

        })
    },
    update(data) {
        return service({
            url: "/api/employee/update",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...t},
            data: data

        })
    }

};
