import {service, t,} from "../../../../http/Request";


/** 公用接口 */
export const equipmentInfoApi = {
    page(data){
        return service({
            url:"/api/equipmentInfo/page",
            method:'get',
            params:data
        });

    },

    getOne(data){
        return service({
            url: "/api/equipmentInfo/get",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

    delete(data) {
        return service({
            url: "/api/equipmentInfo/delete",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

    add(data, params) {
        return service({
            url: "/api/equipmentInfo/insert",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...params,...t},
            data: data

        })
    },
    update(data) {
        return service({
            url: "/api/equipmentInfo/update",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...t},
            data: data

        })
    }
};
