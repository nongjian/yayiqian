import {service, t,} from "../../../../http/Request";


/** 公用接口 */
export const followUpApi = {
    page(data){
        return service({
            url:"/api/followUp/page",
            method:'get',
            params:data
        });

    },



    getOne(data){
        return service({
            url: "/api/followUp/get",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

    delete(data) {
        return service({
            url: "/api/followUp/delete",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

    add(data, params) {
        return service({
            url: "/api/followUp/insert",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...params,...t},
            data: data

        })
    },
    update(data) {
        return service({
            url: "/api/followUp/update",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...t},
            data: data

        })
    }
};
