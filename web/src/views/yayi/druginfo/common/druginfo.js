import {service,t} from "../../../../http/Request";


/** 公用接口 */
export const druginfoApi = {
    list(data){
        return service({
            url:"/api/drugInfo/list",
            method:'get',
            params:data
        });

    },
    page(data){
        return service({
            url:"/api/drugInfo/page",
            method:'get',
            params:data
        });

    },

    getOne(data){
        return service({
            url: "/api/drugInfo/get",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

    delete(data) {
        return service({
            url: "/api/drugInfo/delete",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

    add(data, params) {
        return service({
            url: "/api/drugInfo/insert",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...params,...t},
            data: data

        })
    },
    update(data) {
        return service({
            url: "/api/drugInfo/update",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...t},
            data: data

        })
    }
};
