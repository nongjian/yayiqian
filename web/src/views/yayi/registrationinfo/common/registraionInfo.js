import {service, t,} from "../../../../http/Request";


/** 公用接口 */
export const registraionInfoApi = {
    getLocalTime(data){
        return service({
            url: "/api/registraionInfo/getLocalTime",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

  getListByid(data){
    return service({
        url: "/api/registraionInfo/getListById",
        method: 'get',
        params: {
            ...data,...t
        }
    });
},
    getday(data){
        return service({
            url: "/api/registraionInfo/getSDay",
            method: 'get',
            params: {
                data,...t
            }
        });
    },
    page(data){
        return service({
            url:"/api/registraionInfo/page",
            method:'get',
            params:data
        });
    },
    update(data) {
        return service({
            url: "/api/registraionInfo/update",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...t},
            data: data

        })
    },


    getOne(data){
        return service({
            url: "/api/registraionInfo/get",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

    delete(data) {
        return service({
            url: "/api/registraionInfo/delete",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

    add(data) {
        return service({
            url: "/api/registraionInfo/insert",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            data: data

        })
    },

};
