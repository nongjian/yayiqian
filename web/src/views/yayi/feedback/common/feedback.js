import {service, t,} from "../../../../http/Request";


/** 公用接口 */
export const feedbackApi = {
    page(data){
        return service({
            url:"/api/feedback/page",
            method:'get',
            params:data
        });

    },


    getOne(data){
        return service({
            url: "/api/feedback/get",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

    delete(data) {
        return service({
            url: "/api/feedback/delete",
            method: 'get',
            params: {
                ...data,...t
            }
        });
    },

    add(data, params) {
        return service({
            url: "/api/feedback/insert",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...params,...t},
            data: data

        })
    },
    update(data) {
        return service({
            url: "/api/feedback/update",
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            params: {...t},
            data: data

        })
    }
};
