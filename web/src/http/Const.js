/*jshint esversion: 9 */
import {isProdoction} from '@/common/OtherTools';

const timeout = 13000;  //api请求超时时间
let baseApiURL;  //api原始链接
if(isProdoction()){  //如果是生产环境
    baseApiURL = 'http://127.0.0.1:8087';
}else{
    baseApiURL = 'http://127.0.0.1:8087';
}
export default{
    baseApiURL,
    timeout,
};
