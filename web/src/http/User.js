import {service,} from "./Request";
const t = {
    t:new Date()
}
/** 公用接口 */
const allApi = {
    login(data){
        return service({
            url:"/api/user/login",
            method:'get',
            params: {
                ...data,...t
            }
        });
    },
    register(data,query){
        return service({
            url:"/api/user/register",
            method:'post',
            data: data ,
            params: {
                ...query,...t
            }
        });
    },
    update(data){
        return service({
            url:"/api/user/update",
            method:'post',
            data: data ,
        });
    },

};

export default allApi;
