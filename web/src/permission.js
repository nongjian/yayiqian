/**
 * 路由权限配置
 * 只做路由跳转时的权限验证
 */
import router from '@/router/index';
import {userData as userDataStore} from "@/store/User";
import {getUserData} from "@/action/FormatUserData";
import {
    sysMeluNameMap,
} from "@/router/Common";

/**
 * 免登录
 * 权限白名单
 * 包含路径，和目录名
 *  */
const whiteList = [
    '/login', '/auth-redirect', '/bind', '/register','/404','/401',
    'main-redirect','/main'
];
/**
 * 登录后的白名单
 * 登录之后可以任意访问的白名单
 */
const whiteList_1 = [
    'navigate','yayi/cases','yayi/druginfo','yayi/employee','yayi/equipmentinfo','yayi/feedback','yayi/followup',
    'yayi/performance', 'yayi/patient', 'yayi/prescriptioninfo', 'yayi/registrationinfo',

];

router.beforeEach(async (to, from, next) => {
    const userData = userDataStore();
    let toPath = to.path;
    let toName = to.name;
    /** 如果是白名单中的路由直接放行 */
    if(whiteList.includes(toPath) || whiteList.includes(toName)){
        next();
        return;
    }
    /**
     * 没登录的跳转到登录页面
     */
    let token = userData.userInfo.token;
    if(!token){
        next(`/login?from=${encodeURIComponent(to.fullPath)}`); // 否则跳转到登录页
        return;
    }
    /** 如果没有菜单则先获取用户数据 */
    if(userData.showMenuList.length == 0){
        await getUserData().catch(()=>{});
    }
    /**
     * 如果是白名单中的路由直接放行
     * 登录后的白名单
     *  */
    console.log("当前",toPath,toName)
    if(whiteList_1.includes(toPath) || whiteList_1.includes(toName)
        ||toPath.indexOf("yayi")!==-1||toName.indexOf("yayi")!==-1){
        next();
        return;
    }
    /**
     * 判断用户是否有该目录权限
     * 必须是系统目录的才判断有无权限
     * 如果有权限的才放行
     * 没权限的跳转到401页面
     *  */
    let hasSysMenuConfigMap = userData.hasSysMenuConfigMap;
    if(
        !!sysMeluNameMap[toName] &&
        (
            !hasSysMenuConfigMap[toName] && !hasSysMenuConfigMap[toPath]
        )
    ){
        next(`/401?path=${toPath}`); // 没权限的跳转到401
        return;
    }
    next();
});
