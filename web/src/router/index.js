/**
 * 路由列表部分
 * 所有路由必须先手动写好，然后由后端菜单接口来进行匹配并且指定是否显示
 */
import {createWebHistory, createRouter, createWebHashHistory} from 'vue-router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
//全局进度条的配置
NProgress.configure({
    easing: 'ease', // 动画方式
    speed: 300, // 递增进度条的速度
    showSpinner: false, // 进度环显示隐藏
    trickleSpeed: 200, // 自动递增间隔
    minimum: 0.3, // 更改启动时使用的最小百分比
    parent: 'body', //指定进度条的父容器
});

export const constantRoutes = [
    {
        path: '/mobapp/login',
        name: '/mobapp/login',
        component: () => import('@/views/mobapp/login.vue'),
    },
    {
        path: '/mobapp/index',
        name: '/mobapp/index',
        component: () => import('@/views/mobapp/index.vue'),
    },
    /** 登录注册相关页面 */
    {
        path: '/login',
        component: () => import('@/views/login/index.vue'),
    },

    {
        path: '',
        redirect: '/main/index',
    },
    /** 其他业务相关页面 */
    {
        path: '/main',
        component: () => import('@/layout/main/index.vue'),
        children: [
            /**
             * 重定向页面
             * 用来刷新标签页
             *  */
            {
                path: 'redirect/:path(.*)',
                name: 'main-redirect',
                component: () => import('@/views/redirect/index.vue'),
                meta: {
                    /** 该页面属于此操作页面，但是不算菜单，不允许添加到标签页上 */
                    isMenu: false,
                },
            },
            // {
            //     path: '401',
            //     name:'main-401',
            //     component: () => import('@/views/error/401.vue'),
            //     meta: {
            //         /** 该页面属于此操作页面，但是不算菜单，不允许添加到标签页上 */
            //         isMenu:false,
            //     },
            // },
            {
                path: 'new-tag-page/:sign',
                component: () => import('@/views/system/newTagPage/index.vue'),
                name: 'new-tag-page',
                meta: {
                    isMenu: true,
                },
            },
            /** 一些页面例子 */
            {
                path: 'index',
                component: () => import('@/views/exampleViews/main/index.vue'),
                name: 'main-index',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'show-list',
                component: () => import('@/views/exampleViews/showList/index.vue'),
                name: 'show-list',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'show-list/add',
                component: () => import('@/views/exampleViews/showList/add.vue'),
                name: 'show-list-add',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'show-list/update/:sign',
                component: () => import('@/views/exampleViews/showList/add.vue'),
                name: 'show-list-update',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'show-list/info/:sign',
                component: () => import('@/views/exampleViews/showList/info.vue'),
                name: 'show-list-info',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'user-list',
                component: () => import('@/views/exampleViews/userList/index.vue'),
                name: 'user-list',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'role-list',
                component: () => import('@/views/exampleViews/roleList/index.vue'),
                name: 'role-list',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'icon-list',
                component: () => import('@/views/exampleViews/iconList/index.vue'),
                name: 'icon-list',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'mine/:sign',
                component: () => import('@/views/exampleViews/mine/index.vue'),
                name: 'mine',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'setup-tag',
                component: () => import('@/views/exampleViews/setupTag/index.vue'),
                name: 'setup-tag',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'setup-menu',
                component: () => import('@/views/exampleViews/setupMenu/index.vue'),
                name: 'setup-menu',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'system/menu',
                component: () => import('@/views/system/menu/index.vue'),
                name: 'menu',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'system/bt-permission',
                component: () => import('@/views/system/btPermission/index.vue'),
                name: 'bt-permission',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'iframe/:sign(.*)',
                component: () => import('@/views/system/iframe/index.vue'),
                name: 'iframe',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'other-view',
                component: () => import('@/views/exampleViews/otherView/index.vue'),
                name: 'other-view',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'merge-table',
                component: () => import('@/views/exampleViews/mergeTable/index.vue'),
                name: 'merge-table',
                meta: {
                    isMenu: true,
                },
            },

            {
                path: 'yayi/cases',
                component: () => import('@/views/yayi/cases/index.vue'),
                name: 'yayi/cases',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/cases/update/:sign/:type',
                component: () => import('@/views/yayi/cases/add.vue'),
                name: 'yayi/cases/update',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/druginfo',
                component: () => import('@/views/yayi/druginfo/index.vue'),
                name: 'yayi/druginfo',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/druginfo/update/:sign/:type',
                component: () => import('@/views/yayi/druginfo/add.vue'),
                name: 'yayi/druginfo/update',
                meta: {
                    isMenu: true,
                },
            },
            // 员工
            {
                path: 'yayi/employee',
                component: () => import('@/views/yayi/employee/index.vue'),
                name: 'yayi/employee',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/employee/update/:sign/:type',
                component: () => import('@/views/yayi/employee/add.vue'),
                name: 'yayi/employee/update',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/equipmentinfo',
                component: () => import('@/views/yayi/equipmentinfo/index.vue'),
                name: 'yayi/equipmentinfo',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/equipmentinfo/update/:sign/:type',
                component: () => import('@/views/yayi/equipmentinfo/add.vue'),
                name: 'yayi/equipmentinfo/update',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/feedback',
                component: () => import('@/views/yayi/feedback/index.vue'),
                name: 'yayi/feedback',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/feedback/update/:sign/:type',
                component: () => import('@/views/yayi/feedback/add.vue'),
                name: 'yayi/feedback/update',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/followup',
                component: () => import('@/views/yayi/followup/index.vue'),
                name: 'yayi/followup',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/followup/update/:sign/:type',
                component: () => import('@/views/yayi/followup/add.vue'),
                name: 'yayi/followup/update',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/performance',
                component: () => import('@/views/yayi/performance/index.vue'),
                name: 'yayi/performance',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/performance/update/:sign/:type',
                component: () => import('@/views/yayi/performance/add.vue'),
                name: 'yayi/performance/update',
                meta: {
                    isMenu: true,
                },
            },
            //患者
            {
                path: 'yayi/patient',
                component: () => import('@/views/yayi/patient/index.vue'),
                name: 'yayi/patient',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/patient/update/:sign/:type',
                component: () => import('@/views/yayi/patient/add.vue'),
                name: 'yayi/patient/update',
                meta: {
                    isMenu: true,
                },
            },


            {
                path: 'yayi/prescriptioninfo',
                component: () => import('@/views/yayi/prescriptioninfo/index.vue'),
                name: 'yayi/prescriptioninfo',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/prescriptioninfo/update/:sign/:type',
                component: () => import('@/views/yayi/prescriptioninfo/add.vue'),
                name: 'yayi/prescriptioninfo/update',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/registrationinfo',
                component: () => import('@/views/yayi/registrationinfo/index.vue'),
                name: 'yayi/registrationinfo',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/registrationinfo/update/:sign/:type',
                component: () => import('@/views/yayi/registrationinfo/add.vue'),
                name: 'yayi/registrationinfo/update',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/echart1',
                component: () => import('@/views/yayi/echart/index.vue'),
                name: 'yayi/echart1',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'yayi/echart2',
                component: () => import('@/views/yayi/echart/index2.vue'),
                name: 'yayi/echart2',
                meta: {
                    isMenu: true,
                },
            },
        ],
    },
    {
        path: '/big-screen',
        component: () => import('@/layout/bigScreen/index.vue'),
        children: [
            /**
             * 重定向页面
             * 用来刷新标签页
             *  */
            {
                path: 'redirect/:path(.*)',
                name: 'big-screen-redirect',
                component: () => import('@/views/redirect/index.vue'),
                meta: {
                    /** 该页面属于此操作页面，但是不算菜单，不允许添加到标签页上 */
                    isMenu: false,
                },
            },
            {
                path: 'show_1',
                component: () => import('@/views/bigScreen/show_1/index.vue'),
                name: 'big-screen-show_1',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'show_2',
                component: () => import('@/views/bigScreen/show_2/index.vue'),
                name: 'big-screen-show_2',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'show_3',
                component: () => import('@/views/bigScreen/show_3/index.vue'),
                name: 'big-screen-show_3',
                meta: {
                    isMenu: true,
                },
            },
            {
                path: 'show_4',
                component: () => import('@/views/bigScreen/show_4/index.vue'),
                name: 'big-screen-show_4',
                meta: {
                    isMenu: true,
                },
            },
        ],
    },
    /** 404页面 */
    {
        path: "/:pathMatch(.*)*",
        component: () => import('@/views/error/404.vue'),
    },
    /** 401页面 */
    {
        path: '/401',
        component: () => import('@/views/error/401.vue'),
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes: constantRoutes,
});

/** 此处只添加路由进度条动画 */
router.beforeEach((to, from, next) => {
    NProgress.start();
    // console.log(to.fullPath,from.fullPath,next)
    //
    // if (to.fullPath != '/mobapp' && document.body.clientWidth < 500) {
    //     console.log("手机端")
    //     next({path: '/http://localhost:8087/main/yayi/registrationinfo'})
    // } else if (to.fullPath === '/mobapp' && document.body.clientWidth > 500) {
    //     console.log("电脑端")
    //     next({path: ''})
    // } else {
    //     next();
    // }
    next();
});
router.afterEach(() => {
    NProgress.done();
    /** 清除loading标记 */
    let loadingEl = document.querySelector('#html-loading-el');
    if (loadingEl) {
        loadingEl.remove();
    }
});

export default router;
