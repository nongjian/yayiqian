/**
 * 格式化用户的一些数据
 * 在这里可以接口请求用户的一些数据
 */
import {userData} from "@/store/User";
import {
    sysMeluNameMap,
    sysMeluPathMap,
} from "@/router/Common";
import {toTree,unfoldTreeList} from "@/common/MenuTools";
import {guid} from "@/common/Guid";

/**
 * 转换用户menu
 * 目的是分出用于显示的和用户判断目录权限的
 * 用于判断权限的包含配置信息
 *  */
function transUserMenu(menuList){
    let hasSysMenuConfigMap = {};
    let showMenuList = [];
    /**
     * 将树形结构展开
     * 需要换成一维数组来过滤不要展示的目录
     *  */
    menuList = unfoldTreeList(menuList,{
        childsKey:'childs',
        setParentKey:'parentSign',
        getParentKey:'sign',
        forEachFn(item){
            /** 添加唯一标识以便区分 */
            item.sign = guid();
        },
    });
    menuList.forEach(item=>{
        /**
         * 根据目录配置找到对应的系统menu
         * 添加权限，添加已有的权限列表
         * 因为path属于name的子集，所哟name,path都应该有自己的配置
         * 赋予正确的名称，并添加以path为优先，name次之的权限
         *  */
        let path = item.path;
        let name = item.name;
        if(!!path && !name) {
            /**
             * 有路由地址，但没菜单名称
             *  */
            let sysMenu = sysMeluPathMap[item.path] || {};
            item.name = sysMenu.name;
            hasSysMenuConfigMap[item.path] = item;
        };
        if(!path && !!name) {
            /** 没路由地址，但有菜单名称 */
            let sysMenu = sysMeluNameMap[item.name] || {};
            item.path = sysMenu.path;
            hasSysMenuConfigMap[item.name] = item;
        };
        if(!!path && !!name) {
            /**
             * 有路由地址，有菜单名称
             * 以路由为准
             *  */
            hasSysMenuConfigMap[item.path] = item;
        }
        /** 有唯一标识的也添加，方便查找，可以替换一些信息 */
        if(!!item.sign){
            hasSysMenuConfigMap[item.sign] = {
                ...item,
            };
        }
    });
    showMenuList = menuList.filter(item=>!item.hidden);
    showMenuList = toTree(showMenuList.map(item=>{
        delete item.childs;
        return item;
    }),{
        pKey:'parentSign',
        key:'sign',
        childsKey:'childs',
        isNew:true,
    });
    return {
        showMenuList,
        hasSysMenuConfigMap,
    };
}
/**
 * 获取用户详细数据
 */
export function getUserData(){
    return Promise.resolve().then(()=>{
        const userDataStore = userData();
        let userInfo = userDataStore.userInfo || {};
        /** 写入基本信息  */
        userDataStore.setUserInfo({
            ...userInfo,
            "userName": "admin",
            "nickName": "管理员",
            "avatar":'https://cn.bing.com/th?id=OHR.AdelieWPD_ZH-CN8434233391_1920x1080.jpg&rf=LaDigue_1920x1080.jpg&pid=hp',
        });
        /**
         * 用户目录列表
         * name表示对应的系统目录，有name才有此系统目录的权限
         * 有path的可直接跳转
         * 没path的，根据name获取映射的系统菜单属性进行跳转
         * isCache 表示该页面是否缓存
         * hidden 表示该页面是否在左边目录上显示
         * isLink 表示直接跳转新页面
         *  */
        let menuList = [
            {
                name:"main-index",
                title:'首页',
                content:'',
                isCache:true,
                fixed:true,
                iconName:"all-fill",
            },
            {
                title:'牙科诊所管理',
                iconName:"icon-drag",
                sort:1000,
                childs:[
                    {
                        name:"yayi/employee",
                        title:'员工管理',
                        isCache:true,
                        content:'',
                        iconName:"picside-fill",
                        showTagIcon:true,
                    },
                    {
                        name:"yayi/equipmentinfo",
                        title:'器械管理',
                        isCache:true,
                        content:'',
                        iconName:"piccenter-fill",
                        showTagIcon:true,
                    },
                    {
                        name:"yayi/druginfo",
                        title:'药品管理',
                        isCache:true,
                        content:'',
                        iconName:"paper-plane",
                        showTagIcon:true,
                    },
                    {
                        name:"yayi/feedback",
                        title:'反馈管理',
                        isCache:true,
                        content:'',
                        iconName:"map-fill",
                        showTagIcon:true,
                    },
                    {
                        name:"yayi/followup",
                        title:'随访管理',
                        isCache:true,
                        content:'',
                        iconName:"laptop",
                        showTagIcon:true,
                    },
                    {
                        name:"yayi/cases",
                        title:'病例管理',
                        isCache:true,
                        content:'',
                        iconName:"flag",
                        showTagIcon:true,
                    },
                    {
                        name:"yayi/performance",
                        title:'绩效管理',
                        isCache:true,
                        content:'',
                        iconName:"compress-alt",
                        showTagIcon:true,
                    },
                    {
                        name:"yayi/patient",
                        title:'患者管理',
                        isCache:true,
                        content:'',
                        iconName:"comment-lines",
                        showTagIcon:true,
                    },
                    {
                        name:"yayi/prescriptioninfo",
                        title:'处方管理',
                        isCache:true,
                        content:'',
                        iconName:"chart-scatter-plot",
                        showTagIcon:true,
                    },
                    {
                        name:"yayi/registrationinfo",
                        title:'预约管理',
                        isCache:true,
                        content:'',
                        iconName:"collection-fill",
                        showTagIcon:true,
                    },
                ]
            },
            {
                title:'图表数据',
                iconName:"laptop-check",
                sort:1000,
                childs:[
                    {
                        name:"yayi/echart1",
                        title:'药品消耗',
                        isCache:true,
                        content:'',
                        iconName:"laptop-check",
                        showTagIcon:true,
                    },
                    {
                        name:"yayi/echart2",
                        title:'器械存有量',
                        isCache:true,
                        content:'',
                        iconName:"borderverticle-fill",
                        showTagIcon:true,
                    },
                ]
            },
            // {
            //     name:"new-tag-page",
            //     title:'新标签页',
            //     isCache:true,
            //     hidden:true,
            // },
            // {
            //     title:'系统管理',
            //     iconName:"cog-fill",
            //     childs:[
            //         {
            //             name:"menu",
            //             title:'菜单管理',
            //             isCache:true,
            //             content:'用户目录配置',
            //             iconName:"alignleft-fill",
            //             showTagIcon:true,
            //         },
            //         {
            //             name:"bt-permission",
            //             title:'按钮权限管理',
            //             isCache:true,
            //             content:'与菜单分开',
            //             iconName:"borderverticle-fill",
            //             showTagIcon:true,
            //         },
            //         {
            //             name:"user-list",
            //             title:'用户管理',
            //             isCache:true,
            //             content:'',
            //             iconName:"user-fill",
            //             showTagIcon:true,
            //         },
            //         {
            //             name:"role-list",
            //             title:'角色列表',
            //             isCache:true,
            //             content:'',
            //             iconName:"user-group-fill",
            //             showTagIcon:true,
            //         },
            //     ],
            // },

            // {
            //     name:"show-list",
            //     title:'展示列表',
            //     iconName:"laptop-check",
            //     number:4,
            // },
            // {
            //     name:"merge-table",
            //     title:'合并表格展示列表',
            //     iconName:"laptop-check",
            // },
            // {
            //     name:"other-view",
            //     title:'其他功能展示',
            //     iconName:"map-fill",
            //     isCache:true,
            // },
            // {
            //     title:'大屏展示',
            //     content:'多种方式',
            //     iconName:"laptop",
            //     childs:[
            //         {
            //             title:'示例1',
            //             isCache:true,
            //             content:'使用缩放',
            //             iconName:"laptop",
            //             isLink:true,
            //             path:'/big-screen/show_1',
            //         },
            //         {
            //             title:'示例2',
            //             isCache:false,
            //             content:'使用Rem',
            //             iconName:"laptop",
            //             isLink:true,
            //             path:'/big-screen/show_2',
            //         },
            //         {
            //             title:'示例3',
            //             isCache:true,
            //             content:'固定宽高',
            //             iconName:"laptop",
            //             isLink:true,
            //             path:'/big-screen/show_3',
            //         },
            //         {
            //             title:'示例4(推荐)',
            //             isCache:true,
            //             content:'宽高缩放',
            //             iconName:"laptop",
            //             isLink:true,
            //             path:'/big-screen/show_4',
            //         },
            //     ],
            // },
            // {
            //     name:"show-list-info",
            //     title:'数据详情',
            //     hidden:true,
            //     iconName:"all-fill",
            // },
            // {
            //     name:"show-list-add",
            //     title:'数据添加',
            //     hidden:true,
            //     iconName:"Navbar-full",
            // },
            // {
            //     name:"show-list-update",
            //     title:'数据编辑',
            //     hidden:true,
            //     isCache:true,
            //     content:'(有缓存)',
            //     iconName:"Navbar-full",
            // },
            // {
            //     title:'多级菜单',
            //     iconName:"alignleft-fill",
            //     childs:[
            //         {
            //             title:'可点击父级',
            //             path:'/main/show-list/update/erterter',
            //             iconName:"aligncenter-fill",
            //             childs:[
            //                 {
            //                     name:"show-list-update",
            //                     path:'/main/show-list/update/123123',
            //                     title:'可点击父级',
            //                     iconName:"aligncenter-fill",
            //                     childs:[
            //                         {
            //                             name:"show-list-update",
            //                             path:'/main/show-list/update/1231233',
            //                             title:'数据编辑 - 测试',
            //                             iconName:"test-1",
            //                             showTagIcon:true,
            //                         },
            //                     ],
            //                 },
            //                 {
            //                     title:'父级',
            //                     iconName:"aligncenter-fill",
            //                     childs:[
            //                         {
            //                             name:"show-list-update",
            //                             path:'/main/show-list/update/1235123',
            //                             title:'数据编辑 - 测试1',
            //                             iconName:"plus-square-fill",
            //                         },
            //                     ],
            //                 },
            //             ],
            //         },
            //     ],
            // },
            // {
            //     name:"icon-list",
            //     title:'icon 列表展示',
            //     isCache:true,
            //     content:'(有缓存)',
            //     // iconName:"collection-fill",
            //     iconName:"logo",
            //     showTagIcon:true,
            //     number:20,
            // },
            {
                name:"mine",
                title:'个人中心',
                isCache:true,
                content:'(有缓存)',
                hidden:true,
                iconName:"Navbar-full",
            },
            // {
            //     name:"setup-tag",
            //     title:'设置标签页',
            //     isCache:true,
            //     content:'(有缓存)',
            //     hidden:false,
            //     iconName:"tag",
            // },
            // {
            //     name:"setup-menu",
            //     title:'目录信息',
            //     isCache:true,
            //     content:'(有缓存)',
            //     hidden:false,
            //     iconName:"Directory-tree",
            // },

        ];
        let transData = transUserMenu(menuList);
        /** 写入展示菜单数据 */
        userDataStore.setShowMenuList(transData.showMenuList);
        /** 写入权限菜单数据 */
        userDataStore.setHasSysMenuConfigMap(transData.hasSysMenuConfigMap);
    });
}
/**
 * 获取用户详细数据
 */
export function getUserData_1(){
    return Promise.resolve().then(()=>{
        const userDataStore = userData();
        let userInfo = userDataStore.userInfo || {};
        /** 写入基本信息  */
        userDataStore.setUserInfo({
            ...userInfo,
            "userId": "1",
            "userName": "admin",
            "nickName": "管理员",
            "email": "admin@163.com",
            "phonenumber": "15888888888",
            "admin": true,
        });
        /**
         * 用户目录列表
         * 有path的可直接跳转
         * 没path的，根据name获取映射的系统菜单属性进行跳转
         * isCache 表示该页面是否缓存
         * hidden 表示该页面是否在左边目录上显示
         * isLink 表示直接跳转新页面
         *  */
        let menuList = [
            {
                name:"main-index",
                title:'首页',
                isCache:false,
                iconName:"all-fill",
                number:23,
            },
            {
                name:"icon-list",
                title:'icon 列表展示',
                fixed:true,
                isCache:true,
                content:'(有缓存，并且标签页固定)',
                iconName:"collection-fill",
                number:10,
            },
            {
                name:"show-list",
                title:'展示列表',
                iconName:"laptop-check",
                number:2,
            },
            {
                name:"user-list",
                title:'用户列表',
                isCache:false,
                iconName:"database",
            },
            {
                name:"setup-tag",
                title:'设置标签页',
                hidden:false,
                iconName:"tag",
            },
            {
                name:"setup-menu",
                title:'目录信息 - 更新版',
                isCache:false,
                hidden:false,
                iconName:"Directory-tree",
            },
        ];
        let transData = transUserMenu(menuList);
        /** 写入展示菜单数据 */
        userDataStore.setShowMenuList(transData.showMenuList);
        /** 写入权限菜单数据 */
        userDataStore.setHasSysMenuConfigMap(transData.hasSysMenuConfigMap);
    });
}
/**
 * 写入目录信息
 * 由外部指定
 *  */
export function setMenuData(treeList){
    const userDataStore = userData();
    let menuList = treeList;
    let transData = transUserMenu(menuList);
    /** 写入展示菜单数据 */
    userDataStore.setShowMenuList(transData.showMenuList);
    /** 写入权限菜单数据 */
    userDataStore.setHasSysMenuConfigMap(transData.hasSysMenuConfigMap);
}
/**
 * 用户退出登录
 * 清空用户数据
 */
export function logout(){
    const userDataStore = userData();
    userDataStore.setUserInfo({});
    userDataStore.setShowMenuList([]);
    userDataStore.setHasSysMenuConfigMap({});
    userDataStore.setTagList([]);
    userDataStore.setActiveSign('');
}
